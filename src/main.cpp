#include <M5Core2.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

/* use DHT22 */
#define DHTPIN 33     // Digital pin connected to the DHT sensor 
#define DHTTYPE DHT22 // configure DHT22
DHT dht(DHTPIN, DHTTYPE);

/* use wifi to retrieve datetime */
// My credentials
const char* ssid     = "DorineB";
const char* password = "2d4782a052b6";

// Define NTP Client to get time
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

// Variables to save date and time
String formattedDate;
String dayStamp;
String timeStamp;

RTC_TimeTypeDef TimeStruct;

void configure_message(){
  M5.Lcd.setCursor (0, 5);
  M5.Lcd.setTextSize(3); 
  // M5.Lcd.print("appuyer sur le bouton\n\n");
}

void display_data(float t, float h){
  M5.Lcd.setCursor (0, 5);
  M5.Lcd.printf("Temperature :\n %.2f C\n\n", t);
  M5.Lcd.printf("Humidite :\n %.2f %%\n\n", h);
  M5.Lcd.print("A: ");
  M5.Lcd.print(timeStamp);
}

void retrieve_datetime() {
   /* retrieve datetime and transforme it */
  while(!timeClient.update()){
    timeClient.forceUpdate();
  }
  // The formattedDate comes with the following format:
  // 2018-05-28T16:00:13Z
  // We need to extract date and time
  formattedDate = timeClient.getFormattedTime();

  // Extract date
  int splitT = formattedDate.indexOf("T");
  // dayStamp = formattedDate.substring(0, splitT);
  // Extract time
  // timeStamp = formattedDate.substring(splitT+1, formattedDate.length()-1);
  timeStamp = formattedDate.substring(splitT+1, formattedDate.length());
}

void setup() {
  // init
  M5.begin();
  dht.begin();
  WiFi.begin(ssid, password);
  // Initialize a NTPClient to get time
  timeClient.begin();
  timeClient.setTimeOffset(3600);
  configure_message();
}

void loop() {
  unsigned long ts = millis();
  static unsigned long last_refresh_ts = 0;
  static int del = 0;

  // refresh depending delay
  if(ts > last_refresh_ts + del ){
      last_refresh_ts = ts;
      retrieve_datetime();
      M5.Lcd.clearDisplay();
      float h = dht.readHumidity();
      float t = dht.readTemperature();
      // Check if any reads failed and exit early (to try again).
      if (isnan(h) || isnan(t)) {
        M5.Lcd.setTextSize(3);
        M5.Lcd.println("Lecture impossible !");
        return;
      }
      display_data(t, h);
      configure_message();
  }

  // change the refresh delay is button is pressed 
  if(M5.BtnA.isPressed()){
      del = 1000;
  } else {
      del = 30000;
  }
  
  M5.update();
  delay(10);
}
